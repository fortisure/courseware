/*** Select all people with the last name 'Smith' and concatenate both names into a fullname ***/
SELECT BusinessEntityID, 
	   CONCAT(FirstName, ' ', LastName) as FullName
  FROM Person.Person
  WHERE LastName = 'Smith'