/* Creates a new business entity ID and inserts a new person record */

USE AdventureWorks2017

INSERT INTO Person.BusinessEntity 
    (rowguid) 
    VALUES (NEWID());

INSERT INTO Person.Person
    (
	   BusinessEntityID,
	   PersonType,
	   NameStyle,
	   Title,
	   FirstName,
	   MiddleName,
	   LastName, 
	   Suffix,
	   rowguid
    )
    VALUES
    (
	   (SELECT MAX(BusinessEntityID) FROM Person.BusinessEntity),
	   'EM',
	   0,
	   NULL,
	   'Test',
	   'T',
	   'Patterson',
	   'III',
	   NEWID()
    );