/* Deletes Last Created Person */

USE AdventureWorks2017

DELETE FROM Person.Person 
    WHERE BusinessEntityID = (SELECT MAX(BusinessEntityID) FROM Person.BusinessEntity)