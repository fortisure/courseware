''' 
Make a simple rock-paper-scissors game
Winner wins best two out of three
--------------------------------------
Required functionality:
    - Create a list including ["r", "p", "s"]
    - Also use random number generation within
    - Accept user input for their choice
        - Exit on invalid input
    - Print results of each round 
'''
# Imports the "random" module for random number generation
import random

# Sets a list of possible options
attacks = ["r", "p", "s"]

# Stores the total number of player wins
wins = 0

# Performs a loop to count total number of rounds
# _ means that the particular iteration isn't stored to a variable
    # You could also assign it with a variable name instead
for _ in range(3):
    
    '''
    Generates the computer's attack 
    0 and 2 are the range of list index values
    Lists count up from 0, so 0 = 1, 1 = 2 and 2 = 3
    Selects the attack randomly from the "options" list
    '''
    computer_atk = attacks[random.randint(0, 2)]

    # `raw_input` gets a string from user input, as opposed to `input`, which only receives numbers
    player_atk = input("Choose r, p or s: ")

    # Checks that player input attack is an available attack option
    if player_atk in attacks:

        if computer_atk == player_atk:
            print("Tie!")
        elif player_atk == "r" and computer_atk == "p":
            print("Paper covers rock, you lose!")
        elif player_atk == "r" and computer_atk == "s":
            print("Rock smashes scissors, you win!")
            # Adds 1 to the "wins" variable
            wins += 1
        elif player_atk == "p" and computer_atk == "s":
            print("Scissors cut paper, you lose!")
        elif player_atk == "p" and computer_atk == "r":
            print("Paper covers rock, you win!")
            wins += 1
        elif player_atk == "s" and computer_atk == "r":
            print("Rock smashes scissors, you lose!")
        elif player_atk == "s" and computer_atk == "p":
            print("Scissors cut paper, you win!")
            wins += 1

    else :
        print("Option not recognized! Abort!")
        # Breaks out of the loop
        break

if wins >= 2:
    print("WINNER! You won " + str(wins) + " times!")
else:
    print("Better luck next time, loser. You only won " + str(wins) + " times.")