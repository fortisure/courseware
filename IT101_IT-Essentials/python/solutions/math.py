# Create two variables, x and y and fill them with numbers
x = 16
y = 3

# Print the two added (+), then subtracted (-), then multiplied (*), then divided (/)
print(x + y)
print(x - y)
print(x * y)
print(x / y)

# Print the remainder using the modulus operator (%)
#   NOTE: This will be important for our "FizzBuzz" assignment later
print(x % y)

# Print x to the power of 3 using the ** operator
print(x ** 3)

# Create another variable named z and fill it with a decimal number
z = 3.68

# Create a new `final` variable with the value of 
# a complex set of operations on the other variables
#   NOTE: Order of operations will be respected 
#           by Python, including parentheses
final = x - y ** 4 + (z * (x % 8) / y)

print(final)