# Guess the number

# This example makes use of functions (with parameters), variables, looping, comparisons, user input and printing

# Imports "randint" package for generating random numbers (used in calling the function later)
from random import randint


# Defines a function that accepts the max number of guesses and the magic number as parameters
def guess_number(guesses, number):

    # This is a "for loop" and will repeat the code indented under it the number of times given in "guesses"
        # range(guesses) allows us to "iterate" over the integer that we supplied in the "guesses" parameter
    for i in range(guesses):
        # Asks the user to enter a number and stores it to a "guesses" variable
        guess = int(input("Guess number " + str(i+1) + ": "))

        # If statement checks if certain conditions are fulfilled
        if guess == number:
            # Correctly guessed!
            print("You got it!")
            # Ends the loop
            break
        # "elif" (short for "else if" in many other languages) allows you to define additional conditions to check for
        elif guess > number:
            # If magic number is lower than the number guessed
            print("Lower")
        elif guess < number:
            # If magic number is higher than number guessed
            print("Higher")

        if i + 1 == guesses:
            print("Too bad! The number was " + str(number))
            break

    # This indentation level is outside of the "for" loop, but still inside of the "guessNumber" function

# No more indentation means that we have exited the function definition



# Let's run the function we wrote above
guess_number(5, randint(1, 100))
# In the second parameter, we are feeding it a random number 
# from 1 to 100 for the magic number using randint(1, 100)