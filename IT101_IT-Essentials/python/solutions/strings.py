# Create a string variable called `name` with your name in it
name = "Brad"

# Print out your name variable below using the print() command
print(name)

# Now print "Hello, [your name here]" using the `name` variable from above
print("Hello " + name)

# Change that name variable to something else
name = "George"

# Create a few more variables for different attributes about yourself
# Age, favorite_food, whatever you want!
age = 30
hair_color = "brown"
favorite_food = "steak"

# Now print out a single line that contains all of these variables. 
# Tell a story with them if you want!
print(f"My name is {name}, I'm {age} years old, I have {hair_color} hair and my favorite food is {favorite_food}")