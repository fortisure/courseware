# Create a simple calculator that performs mathematical operations
# (add, subtract, multiply, divide) on a list of numbers in order
# and outputs the result

# I've defined a function for you that gives you a starting point 
def calculate(method, numbers):
    # This is the variable that you'll store the result in
    total = 0

    # This loops through the second parameter of the function and returns
    # `v` as the value and `k` as the index (0, 1, 2, 3, etc)
    for k, x in enumerate(numbers):

        # While staying indented here to stay within the for loop
        # you'll want to check the method (math operation) and then
        # perform the corresponding operation for each number (the `v` variable)
        # and store it in the `total` variable

        if method == "multiply":
            if k == 0:
                total = x
            else:
                total = total * x

        elif method == "add":
            total = total + x

        elif method == "subtract":
            if k == 0:
                total = x
            else:
                total = total - x
                
        elif method == "divide":
            if k == 0:
                total = x
            else:
                total = total / x

    # This returns the total value from the function
    return total



# Let's run the function a few times!

# Should output 15
print(calculate("add", [3, 4, 6, 2]))

# Should output 2
print(calculate("subtract", [10, 3, 4, 1]))

# Should output 800
print(calculate("multiply", [20, 4, 2, 5]))

# Should output 5
print(calculate("divide", [50, 2, 5]))
