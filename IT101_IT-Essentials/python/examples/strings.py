# Pulls the datetime package so we can use it
import datetime

# Creates a string variable called `name`
name = "Billy"
# Creates a variable to capture the current time
time = datetime.datetime.now().time()

# The print function prints a string to the command line
print ("Hi there!")

# We can also print variables using this function
print(name)

# We can "concatenate" strings together with variables by using the + operator
# NOTE: if we don't format the `time` variable using the str() function, it will error
print("Hello " + name + ", the time is " + str(time))

# We can also do more advanced things like string formatting and variable "interpolation"
# Notice how I'm passing in the `name` and `time` variables into the print function
# These will be placed into the string in the order that the `%s` operators are placed in
# `%s` signifies that the variable should be formatted as a string
print("Hello %s, the time is %s" %(name, time,))

# This is a more modern way of doing it, notice the `f` keyword prefixing the string
# and that the variable names are enclosed with curly brackets - {}
print(f"Hello {name}, the time is {time}")

# If you don't want to only evaluate these variables in order, you can do it in a more advanced
# way by binding specific attribute names to specific values and referencing them in the string
print('%(name)s, %(name)s you so fine, you so fine you %(action)s my mind, hey %(name)s' % {'name': name, 'action': 'expand'} )