# `White space` matters a lot in Python
# Indentation determines "block scope"

# Notice these two variables are being initialized with a value of 0
x = 0
y = 0

# This is a code block containing a `for` loop
for i in range(5):
    # Notice the fact that the x += 1 line is one tab over
    # This increments the x variable by 1 each time it is run
    # Since it's indented one tab past the beginning of the `for` loop,
    # it will be repeated as part of the loop 
    # however many times the loop runs
    x += 1
# This line performs the same operation, but on the y variable
# Since it's not indented, it will perform outside of the `for` loop,
# meaning it will only run once
y += 1

# This was incremented 5 times, so the output will be `5`
print(x)
# This only incremented 1 time, so the output will be `1`
print(y)