import sqlite3
import sys
from sqlite3 import Error
 
# Creates connection to sqlite database file
def create_connection(db_file):
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except Error as e:
        print(e)
 
    return None

# Creates string from database rows
def generate_row_string(rows, col_name_list):
    rowstr = ""
    for row in rows:

        # Generates key: value string out of row data
        for idx, colname in enumerate(col_name_list):
                rowstr += str(colname) + ": " + str(row[idx]) + ", "
                
        # Trims ", " from end of row string
        rowstr = rowstr[:-2]
        rowstr += "\n"

    return rowstr
 
# Selects all invoices
def select_all_invoices(conn):
   
    cur = conn.cursor()
    # Executes SQL query
    cur.execute("SELECT * FROM invoices")

    # Fetches rows from SQL query result
    rows = cur.fetchall()
 
    # Gets column names from database table
    col_name_list = [tuple[0] for tuple in cur.description]

    # Generates row string from user-defined-funciton
    rowstr = generate_row_string(rows, col_name_list)
    
    # Writes string to console
    sys.stdout.write(rowstr)
    print("")
 
# Selects details from a specific invoice by invoice ID
def select_invoice_details(conn, invoice):

    cur = conn.cursor()
    cur.execute("SELECT * FROM invoices WHERE invoiceid=?", (invoice,))
 
    # Fetches rows from SQL query result
    rows = cur.fetchall()
 
    # Gets column names from database table
    col_name_list = [tuple[0] for tuple in cur.description]

    # Generates row string from user-defined-funciton
    rowstr = generate_row_string(rows, col_name_list)
    
    # Writes string to console
    sys.stdout.write(rowstr)
    print("")

# Selects line items from a specific invoice by invoice ID
def select_invoice_items(conn, invoice):

    cur = conn.cursor()
    cur.execute("SELECT * FROM invoice_items WHERE invoiceid=?", (invoice,))
 
    # Fetches rows from SQL query result
    rows = cur.fetchall()
 
    # Gets column names from database table
    col_name_list = [tuple[0] for tuple in cur.description]

    # Generates row string from user-defined-funciton
    rowstr = generate_row_string(rows, col_name_list)
    
    # Writes string to console
    sys.stdout.write(rowstr)
    print("")
 

# Main function that gets called immediately upon script execution
# Program entry point
def main():
    # Database file
    database = "assets/sqlitesample.db"

    # Gets invoice ID (integer) from user input
    invoice_id = input("Please enter an invoice ID (integer): ")
 
    # create a database connection
    conn = create_connection(database)
    with conn:

        print("")

        print("1. Select invoice details by Invoice ID:")
        select_invoice_details(conn, invoice_id)

        print("2. Select invoice line items by invoice ID:")
        select_invoice_items(conn,invoice_id)
        
        '''print("3. Select all invoices")
        select_all_invoices(conn)'''

# Helper function to assist with executing this script from a command line
if __name__ == '__main__':
    main()