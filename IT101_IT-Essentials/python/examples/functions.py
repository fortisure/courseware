
# This is a basic function that accepts a parameter "name" and prints a greeting
def say_hello(name):
    print("Hello " + name)
#
# Let's "call" the function we just created by invoking its name and passing "Bill" as the name
say_hello("Bill") # Will print "Hello Bill"



# Functions can also "return" values rather than just printing them directly
def subtract(main_num, sub_num):
    return main_num - sub_num
#
# Let's call the subtract() function now
print( subtract( 10, 4 ) ) # Will print 6
#
# Since it returns a value, we can use the returned value in other ways
#
# Let's define a variable from the returned value of calling subtract twice
small_num = subtract( subtract(100, 30), 40 ) 
# And print the result
print(small_num) # Will print 30



# Functions can also be "recursive" meaning that they call themselves
# 
# If you create a recursive function without specifying an "exit" condition
# you will cause what's called an "infinite loop", or a loop that will run 
# forever (until you force-quit the program)
# 
# Example of a well-designed recursive function
def recursion(x):
    if x <= 10:
        print(x)
        x += 1
        # Function calls itself
        recursion(x)
    # Exit Condition
    else:
        print("Recursion Complete!")
#
# This will print from 1 to 10 and then print "Recursion Complete" as the exit condition
recursion(1)



# A "lambda", also called an "anonymous function" is a 
# shorthand way of defining a function with some additional flexibility
def power(y):
    # arguments : expression
    return lambda x : x**y
#
# Let's create a couple of functions from this lambda function
square = power(2)
cube = power(3)
#
# Now we can call the newly made functions
print ( square(9) ) # Will return 81
print ( cube(4) ) # Will return 64