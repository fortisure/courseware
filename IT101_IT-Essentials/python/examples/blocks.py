##### Code block samples #####

# Variable assignments
x = 1
y = 10

# If statement
#
# An "If statement" is an example of a "conditional" statement.
# Conditionals check the "truthfulness" of a particular assertion
    # For example: 10 > 6 is true. 83 < 4 is false.
if x == y:
    # Notice that the "==" is a conditional check, where "=" assigns a value.
    print("x and y are equal")
# Elif defines an additional specific condition within an if statement
elif x > y:
    print("x is greater than y")
# Else defines a default action when none of the other conditions are met
else:
    print("y is greater than x")

# For loop (over range)
#
# A for loop repeats a given action a specific number of times
# This will print from 0 to 9
for val in range(10):
    print(val)

# While loop
#
# A while loop repeats a given action while a condition remains true
# This will print from the value of x to y (1 to 10)
while y >= x:
    print(x)
    x += 1

# Function definition (multiplies a and b)
def multiply(a, b):
    return a * b

# Let's call the function we just made and print out the value
print( multiply(x + 3, y - 2) ) # Will return 112