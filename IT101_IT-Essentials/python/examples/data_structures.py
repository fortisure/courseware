''' DATA STRUCTURES 

    This script is to show off some of the attributes of each common
    non-primitive data structure and show a small subset of the things that can
    be done with them using built-in methods

PRIMITIVES
    "Primitive" data types are basic building block types in 
    programming languages from which more complexity is built
'''
# Integer
integer_var = 42
# Boolean (True/False)
boolean_var = True
# Floating-point number
float_var = 733.4234168
# String
string_var = "Hello World!"


''' 
COMPLEX DATA TYPES
    "Complex" data types are more complex (go figure) than primitives

    They are usually created from a combination of other primitive data types

SETS 
    Sets are the simplest of the complex data structures

    Each value in a set must be unique

    Values in a set are unordered and unindexed,
    meaning you can't access them by an index value

    Since they are simple and enforce uniqueness, 
    they are the fastest to iterate over
    and have the smallest footprint
'''
set_langs = {"Python", "C++", "Go", "PHP"}
# Since you can't access set items by indexes or keys, 
# they aren't particularly useful for storing values that you need to read frequently
# 
# They are better for storing data that you perform operations on
# and for enforcing uniqueness within your data
#
# For example
# To copy a set to a new set, use .copy() (or just "=")
set_otherlangs = set_langs.copy()
#
# To add a single value to a set, use .add(). To add multiple, use .update
set_langs.update(["Javascript", "C++", "Elixir"]) # This will ignore the duplicate C++ value
#
# To find the difference between two sets, use .difference()
print( set_langs.difference(set_otherlangs) ) # Will return {'Javascript', 'Elixir'}
#
# To find the values in common between two sets, use .intersection()
print( set_langs.intersection(set_otherlangs) ) # Will return {'Python', 'C++', 'Go', 'PHP'}


''' LISTS 
    Lists are similar to "indexed arrays" in other languages

    Lists are ordered and indexed, and they can be changed
'''
list_games = ["Checkers", "Chess", "Keyforge", "Scrabble"]
# Access an item from a list like so (indexes always start with 0)
print( list_games[2] ) # Will return "Keyforge"
#
# Insert an item into a list at a specific position like so
# Doing so will shift the later elements of the list backwards by 1
list_games.insert(2, "Yahtzee")
# Access the same list index as before, but now a new result will be returned
print( list_games[2] ) # Will return "Yahtzee"
#
# Add a new element to the end of a list using .append()
### Note the duplicate value... lists allow this
list_games.append("Chess")
#
# Return the total number of times a particular value occurs in a list
print( list_games.count("Chess") ) # Will return 2
#
# Sort a list in ascending order using .sort()
# Will return ['Checkers', 'Chess', 'Chess', 'Keyforge', 'Scrabble', 'Yahtzee']
list_games.sort() 
print( list_games )
# Sort a list in descending order using .reverse()
# Will return ['Yahtzee', 'Scrabble', 'Keyforge', 'Chess', 'Chess', 'Checkers']
list_games.reverse()
print( list_games )


''' RANGES
    Ranges are immutable (unchangable) groups of numbers 
    typically used for iterating (looping) over

    Ranges are initialized using the range() function that
    accepts 3 parameters: range(start, end [optional], step [optional])

    Ranges always end 1 before the last number (not sure why)
'''
range_months = range(1, 13)
for month_num in range_months:
    print(month_num) # Will print 1 through 12

# If you only pass 1 argument to range, it defaults to starting at 0
range_numbers = range(10)
for number in range_numbers:
    print(number) # Will print 0 through 9


''' DICTS
    Dicts (or dictionaries) are the most complex of these primary structures

    Dicts are unordered and indexed, allowing their 
    contents to be accessed a key string

    Most similar to the concept of an "Associative array" in other languages
'''
dict_agent = {
    "name": "Burt Macklin",
    "age": 35,
    "occupation": "FBI"
}
# Access an item from a dict like so
print( dict_agent["name"] ) # Will return "Burt Macklin"
# 
# Dictionaries can also be more complex, or "multidimensional"
# Notice how this dictionary has several layers, and includes other dictionaries, lists, mixed data types, etc
dict_employees = {
    "sales": {
        1: { 
                "name": "Cindy Sales", 
                "title": "Sales Executive", 
                "skills": { "selling": 10, "losing_the_sale": 0, "winning": 9 } 
            },
        2: { 
                "name": "Ted Bungle", 
                "title": "Generic Sales Guy", 
                "skills": { "selling": 4, "tetris": 10, "avoiding_bosses": 7 } 
            }
    },
    "finance": {
        1: { 
                "name": "Ralph Patterson", 
                "title": "Bean Counter", 
                "hobbies": ["Math", "Calculators", "Bending Paper Clips"] 
            }
    },
    "information_technology": {
        1: { 
                "name": "Brad Magyar", 
                "title": "Instructor", 
                "personality": False
            },
        2: { 
                "name": "Brandon Taylor", 
                "title": "Instructor", 
                "big_nerd": True
            }
    }
}
# Print the name of the 1st employee sales like so:
print( dict_employees["sales"][1]["name"] ) # Will return "Cindy Sales"
# Alternatively, you can use the .get() method
print ( dict_employees.get("information_technology") ) # This will return all IT employees


''' TUPLES 
    Tuples are another simple primary data structure similar to lists,
    but are immutable, meaning that their contents cannot be changed

    There are a few advantages to this, including increased speed 
    when seeking/sorting/iterating through them, as well as 
    It also provides a bit more safety with preserving data that won't change

    Tuples are ordered and indexed, and you can access their values by index number
'''
tuple_colors = ("red", "yellow", "orange", "green")
# Access an item from a tuple like so
print( tuple_colors[1] ) # Will return "yellow"
# Access an item from the end of a tuple (or list) by using a negative index
print ( tuple_colors[-1] ) # Will return "green"
#
# Tuples can be "unpacked" into multiple variables
# Number of variables must match the number of items in tuple
c1, c2, c3, c4 = tuple_colors
print( c3 ) # Will return "orange"
#
# A list composed of tuples, can also be used to create dictionaries
# First, we initialize a list of tuples
tuple_list_color_hex = [("red", "#ff0000"), ("yellow", "#ffff00"), ("orange", "#ffaa00"), ("green", "#00ff00")]
# Then we use the dict() function to create a dictionary from this list of tuples
dict_colors = dict(tuple_list_color_hex)
print(dict_colors) # Will return {'red': '#ff0000', 'yellow': '#ffff00', 'orange': '#ffaa00', 'green': '#00ff00'}