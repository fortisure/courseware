'''
Object-oriented programming

A `class` is a template that provides the definition 
of any `object` that is created from it. 

Classes may have various different types of members
associated with them: constants/variables called `properties`
and functions called `methods`

When a class is instantiated, this is called an "object."
Objects contain all of the associated methods and properties
that are defined within their parent class.

Object-oriented programming can get very complex, but
we'll keep it simple for the sake of this instruction
'''

# This is the class definition.
# We will make an Animal class with certain traits
# and is capable of doing a few things
class Animal():

    # Class variables are called properties
    kind = None
    name = None
    color = None
    speech = None

    # This special function (method) is called
    # an "initializer," and automatically runs
    # when the object is created, and any parameters
    def __init__(self, k, n, c):
        self.kind = k
        self.name = n
        self.color = c
        # Sets what the animal says based on its kind
        if self.kind == "dog":
            self.speech = "woof"
        elif self.kind == "cat":
            self.speech = "meow"
        elif self.kind == "fox":
            self.speech = "ring-ding-ding-ding-dingeringeding"
        else:
            self.speech = "nothing"

    # This is a method. In Python, in order to reference
    # data from the same class, you must pass "self" as
    # the first parameter. Notice how we're referring to
    # The properties defined above.
    def talk(self):
        print(self.speech.capitalize() + "!")

    def talkRepeatedly(self, x):
        for _ in range(x):
            # You can even invoke a method from the same class
            self.talk()
    
    def talkTo(self, otherAnimal):
        print(self.name + " says '" + self.speech + "' to " + otherAnimal.name)

    def getProperties(self):
        print("This " + self.color + " " + self.kind + "'s name is " + self.name)


# Now that we've created the class, let's do some stuff with it!

# When you "instantiate" a class, that is called an Object
# Here we create a new object 
doge = Animal("dog", "Sparky", "brown")

# We can use one of the methods that we defined above
# This will print "Woof!" three times
doge.talkRepeatedly(3)

# Will print a sentence referencing all of the 
# characteristics of the animal object that we created
doge.getProperties()

# We can re-use the class definition above 
# to make as many new objects as we want!
foxy = Animal("fox", "Foxy", "red")
foxy.getProperties()
# We find out what the fox says
foxy.talk()

# This passes the foxy object into the doge
# object and properties from both are accessed.
# Notice that the sentence printed uses data from both objects
# (This will fail if we don't have two animal objects)
doge.talkTo(foxy)
