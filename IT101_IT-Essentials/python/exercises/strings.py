# Create a string variable called `name` with your name in it

# Print out your name variable below using the print() command

# Now print "Hello, [your name here]" using the `name` variable from above

# Change that name variable to something else

# Create a few more variables for different attributes about yourself
# Age, favorite_food, whatever you want!

# Now print out a single line that contains all of these variables. 
# Tell a story with them if you want!
