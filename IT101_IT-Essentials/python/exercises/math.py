# Create two variables, x and y and fill them with numbers


# Print the two added (+), then subtracted (-), then multiplied (*), then divided (/)


# Print the remainder using the modulus operator (%)
#   NOTE: This will be important for our "FizzBuzz" assignment later


# Print x to the power of 3 using the ** operator


# Create another variable named z and fill it with a decimal number


# Create a new `final` variable with the value of 
# a complex set of operations on the other variables
#   NOTE: Order of operations will be respected 
#           by Python, including parentheses
