# Create a `top` variable with a value of 10

# Make a `for` loop that counts from 1 to the `top`
#   variable and prints the current number each time
# You'll need to wrap `top` in a range() function
#   NOTE: Ranges start with 0, so you'll have to add 1 to 
#           the current value when printing


# Do the same thing as above with a `while` loop
# Create a `count` variable with a value of 1

# Create a `while` loop that repeats until the `top` variable is matched by `current`
# Print the current number as in the above example and add 1 to the `current` variable
#
#   NOTE: If you do this wrong, the while loop will never evaluate as false
#   and you'll cause an "infinite loop", which will crash the program and possibly your computer!


# Create an array of fruits

# Create a `for` loop that iterates over this `fruits` array and prints each fruit
