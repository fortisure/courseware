# Create two variables - x and y, and fill them with numbers
# Note: DON'T enclose numbers in quotes


# Make an `if` statement comparing the two variables and print 
#   "true" if the expression evaluates as `true`
# Also create an `else` statement to handle the `false` scenario
#   and print out a string that says "false"


# Make a new variable `n` and fill it with a number

# Create a new if statement to evaluate `n` with three conditions: 
#   One for positive numbers
#   One for negative numbers (use `elif` for this condition)
#   One for zero (use `else` for this condition)
# Print out different strings for each condition

