# Guess the number

# This example makes use of functions (with parameters), variables, looping, comparisons, user input and printing

# Imports "randint" package for generating random numbers (used in calling the function later)
from random import randint


# Defines a function that accepts the max number of guesses and the magic number as parameters
def guess_number(guesses, number):

    



# Let's run the function we wrote above

# In the second parameter, we are feeding it a random number 
# from 1 to 100 for the magic number using randint(1, 100)
guess_number(5, randint(1, 100))