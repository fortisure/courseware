# FizzBuzz!
# The goal of FizzBuzz is to print different pieces of text based on number multiples
# If the number is a multiple of 3, print "Fizz"
# If the number is a multiple of 5, print "Buzz"
# If it is a multiple of both 3 and 5, print "FizzBuzz

# This example should use a for loop (over a range from 1 to 100),
# if + elif statements with comparison and logical operators, and printing

