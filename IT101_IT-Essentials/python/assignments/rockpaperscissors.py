''' 
Make a simple rock-paper-scissors game
Winner wins best two out of three
--------------------------------------
Required functionality:
    - Create an list including ["r", "p", "s"]
    - Also use random number generation within
    - Accept user input for their choice
        - Exit on invalid input
    - Print results of each round 
'''

import random