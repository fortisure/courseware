USE AdventureWorks2017;

DECLARE
    @currencies TABLE
                (
                    CurrencyCode varchar(6)   NOT NULL,
                    CurrencyName varchar(100) NOT NULL,
                    CountryCode  varchar(6)   NOT NULL,
                    CountryName  varchar(100) NOT NULL
                )
DECLARE
    @maxModifiedDates TABLE
                      (
                          CurrencyCode varchar(6) NOT NULL,
                          ModifiedDate datetime   NOT NULL
                      )

INSERT INTO @currencies
SELECT DISTINCT c.CurrencyCode, c.Name as CurrencyName, crc.CountryRegionCode, cr.Name as CountryName
FROM Sales.CountryRegionCurrency crc
         INNER JOIN Person.CountryRegion cr on cr.CountryRegionCode = crc.CountryRegionCode
         INNER JOIN Sales.Currency c on c.CurrencyCode = crc.CurrencyCode;

INSERT INTO @maxModifiedDates
SELECT ToCurrencyCode, MAX(ModifiedDate)
FROM Sales.CurrencyRate
GROUP BY ToCurrencyCode;

SELECT DISTINCT c.CurrencyCode, c.CurrencyName, c2.CurrencyCode, c2.CurrencyName, r.EndOfDayRate, r.ModifiedDate
FROM @currencies c
         INNER JOIN Sales.CurrencyRate r on r.FromCurrencyCode = c.CurrencyCode
         INNER JOIN @currencies c2 on c2.CurrencyCode = r.ToCurrencyCode
         INNER JOIN @maxModifiedDates mmd on mmd.CurrencyCode = r.ToCurrencyCode
WHERE r.ModifiedDate = mmd.ModifiedDate
  AND r.ToCurrencyCode = mmd.CurrencyCode
ORDER BY c2.CurrencyCode