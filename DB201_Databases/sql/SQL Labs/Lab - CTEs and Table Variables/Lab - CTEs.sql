USE AdventureWorks2017;

WITH StateIdCTE AS (
    SELECT DISTINCT StateProvinceID, StateProvinceCode
    FROM Person.CountryRegion cr
             INNER JOIN Person.StateProvince sp on sp.CountryRegionCode = cr.CountryRegionCode
    WHERE cr.Name = 'United States'
)
SELECT p.FirstName, p.LastName, a.AddressLine1, a.AddressLine2, a.City, cte.StateProvinceCode, a.PostalCode
FROM Person.Person p
         INNER JOIN Person.BusinessEntityAddress bea on bea.BusinessEntityID = p.BusinessEntityID
         INNER JOIN Person.Address a on a.AddressID = bea.AddressID
         INNER JOIN StateIdCTE cte on cte.StateProvinceID = a.StateProvinceID
ORDER BY cte.StateProvinceCode, LastName, FirstName