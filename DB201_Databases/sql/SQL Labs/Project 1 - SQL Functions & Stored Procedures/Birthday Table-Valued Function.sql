/* This function will pull a list of all employees with a birthday in the given month */

USE AdventureWorks2017

CREATE FUNCTION HumanResources.f_EmployeeBirthdays(
  @month int
) RETURNS TABLE
  AS
  RETURN
          (
            SELECT p.FirstName, p.LastName, e.BirthDate, ea.EmailAddress, e.BusinessEntityID
            FROM HumanResources.Employee e
                   INNER JOIN Person.Person p ON p.BusinessEntityID = e.BusinessEntityID
                   INNER JOIN Person.EmailAddress ea ON ea.BusinessEntityID = e.BusinessEntityID
            WHERE MONTH(e.BirthDate) = @month
          )

/*
  Now, to run the function, all we need to do is call it within a select statement
  with its fully qualified name (normal rules of select statements apply)

SELECT * FROM HumanResources.f_EmployeeBirthdays(7)
ORDER BY DAY(BirthDate) ASC

*/