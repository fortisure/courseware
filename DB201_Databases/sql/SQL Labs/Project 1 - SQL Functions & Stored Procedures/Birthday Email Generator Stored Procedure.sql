CREATE PROCEDURE HumanResources.s_EmployeeBirthdayEmailGenerator @month int
AS
BEGIN
  SET NOCOUNT ON;

  INSERT INTO HumanResources.EmployeeEmailQueue (ToEmail, Subject, Body, SendDate)

  SELECT EmailAddress,
         'Happy Birthday!',
         'Happy ' + CAST(DATEDIFF(YEAR, BirthDate, GETDATE()) as VARCHAR) + ', ' + FirstName +
         '! We hope it''s a great one!',
         DATEADD(YEAR, YEAR(GETDATE()) - YEAR(BirthDate), BirthDate)
  FROM HumanResources.f_EmployeeBirthdays(@month);

END