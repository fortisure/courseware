USE AdventureWorks2017

CREATE TABLE HumanResources.EmployeeEmailQueue
(
	Id int identity(1, 1) primary key,
	ToEmail nvarchar(100) not null,
	Subject nvarchar(255) not null,
	Body nvarchar(max) not null,
	SendDate date not null
)
GO