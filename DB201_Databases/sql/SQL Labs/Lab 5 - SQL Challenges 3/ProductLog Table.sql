USE AdventureWorks2017;

DROP TABLE IF EXISTS Production.ProductLog;

CREATE TABLE Production.ProductLog (
  ID int IDENTITY (1, 1) PRIMARY KEY,
  ProductId int,
  FieldName varchar(60),
  OldValue nvarchar(50),
  NewValue nvarchar(50),
  Timestamp datetime DEFAULT CURRENT_TIMESTAMP
)