ALTER TRIGGER Production.ProductLogLimit ON Production.ProductLog
  AFTER INSERT
  AS

  DECLARE @productid int = (SELECT ProductID FROM inserted);

  DELETE
    FROM Production.ProductLog
  WHERE ProductID = @productid
    AND ID NOT IN (SELECT TOP 3 Id FROM Production.ProductLog where ProductID = @productid ORDER BY Timestamp desc)

