CREATE TRIGGER Production.ProductLogChanges ON Production.Product
AFTER UPDATE
AS

  DECLARE @productid int = (SELECT ProductID FROM inserted);
  DECLARE @oldname varchar(50), @newname varchar(50), @oldlist varchar(50), @newlist varchar(50);

  SELECT @oldname = Name, @oldlist = ListPrice FROM deleted;
  SELECT @newname = Name, @newlist = ListPrice FROM inserted;


  IF (@oldname != @newname)
    BEGIN
      INSERT INTO Production.ProductLog (ProductId, FieldName, OldValue, NewValue) values (@productid, 'Name', @oldname, @newname);
    END

  IF (@oldlist != @newlist)
    BEGIN
      INSERT INTO Production.ProductLog (ProductId, FieldName, OldValue, NewValue) values (@productid, 'ListPrice', @oldlist, @newlist);
    END
GO