/*** Select all customers with total purchases exceeding $100k (with name & address)  ***/
USE AdventureWorks2017;

SELECT p.FirstName,
       p.LastName,
       CONVERT(DECIMAL(15,2), ROUND(SUM(soh.TotalDue), 2, 1)) as TotalPurchaseAmount,
       a.AddressLine1,
       a.AddressLine2,
       a.City,
       a.PostalCode
FROM Sales.SalesOrderHeader soh
       INNER JOIN Sales.Customer c on c.CustomerID = soh.CustomerID
       INNER JOIN Person.Person p on p.BusinessEntityID = c.PersonID
       INNER JOIN Person.Address a on a.AddressID = soh.BillToAddressID
  /* Statuses - 1 = In process; 2 = Approved; 3 = Backordered; 4 = Rejected; 5 = Shipped; 6 = Cancelled */
WHERE soh.Status = 5
GROUP BY c.PersonID,
         p.FirstName,
         p.LastName,
         a.AddressLine1,
         a.AddressLine2,
         a.City,
         a.PostalCode
HAVING SUM(soh.TotalDue) > 100000
ORDER BY TotalPurchaseAmount DESC