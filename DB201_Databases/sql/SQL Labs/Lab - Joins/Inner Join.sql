/**
 Select employee names (join on Person.Person) and their departments (use BusinessEntityId as key)
 */

USE AdventureWorks2017

SELECT p.FirstName, p.LastName, d.Name as Department
FROM Person.Person p
       INNER JOIN HumanResources.EmployeeDepartmentHistory e on e.BusinessEntityID = p.BusinessEntityID
       INNER JOIN HumanResources.Department d on d.DepartmentID = e.DepartmentID
  ORDER BY LastName;


-- Bonus points if you can figure out how to get one row per person (multiple departments in one cell, if applicable)
-- Use the STRING_AGG() function and grouping to accomplish this
/*

USE AdventureWorks2017

SELECT p.FirstName, p.LastName, STRING_AGG(d.Name, ', ') as Department
FROM Person.Person p
       INNER JOIN HumanResources.EmployeeDepartmentHistory e on e.BusinessEntityID = p.BusinessEntityID
       INNER JOIN HumanResources.Department d on d.DepartmentID = e.DepartmentID
GROUP BY p.FirstName, p.LastName;

*/