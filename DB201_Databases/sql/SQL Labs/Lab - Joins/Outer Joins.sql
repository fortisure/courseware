/*
 Select a list of salespeople with their names, related territory groups and territory names,
 and also include any sales people that do not have assigned territories

 The tables you'll need are Sales.SalesPerson, Person.Person and Sales.SalesTerritory
 */

USE AdventureWorks2017

SELECT p.FirstName, p.LastName, st.[Group] as TerritoryGroup, st.Name as Territory
FROM Sales.SalesPerson sp
       INNER JOIN Person.Person p on p.BusinessEntityID = sp.BusinessEntityID
       LEFT OUTER JOIN Sales.SalesTerritory st on st.TerritoryID = sp.TerritoryID
ORDER BY p.BusinessEntityID


-- Try reversing the order of the tables in the above query and achieving the same result with a RIGHT OUTER JOIN

USE AdventureWorks2017

SELECT p.FirstName, p.LastName, st.[Group] as TerritoryGroup, st.Name as Territory
FROM  Sales.SalesTerritory st
       RIGHT OUTER JOIN Sales.SalesPerson sp on sp.TerritoryID = st.TerritoryID
       INNER JOIN Person.Person p on p.BusinessEntityID = sp.BusinessEntityID
ORDER BY p.BusinessEntityID


-- Now try to write a query utilizing a FULL OUTER JOIN that pulls in Sales.SalesOrderHeader info and Sales.SalesPerson
-- Include orders with no salesperson and salesperson with no orders attached

USE AdventureWorks2017

SELECT soh.SalesOrderID, soh.TotalDue, soh.OrderDate, p.FirstName + ' ' + p.LastName as SalesPersonName FROM Sales.SalesOrderHeader soh
       FULL OUTER JOIN Sales.SalesPerson sp on sp.BusinessEntityID = soh.SalesPersonID
       FULL OUTER JOIN Person.Person p on p.BusinessEntityID = sp.BusinessEntityID


