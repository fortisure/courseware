/*
 Create a CTE that tracks colors (store color Id and color Name and add a few to the table when complete)
 Then use CROSS JOIN to create color variants of each product

 You can use this as the CTE

 WITH Colors (Id, Name) AS (SELECT 1 as Id, 'Blue' as Name UNION ALL SELECT 2, 'Red' UNION ALL SELECT 3, 'Green' UNION ALL SELECT 4, 'Yellow')
 */

USE AdventureWorks2017;

WITH Colors (Id, Name) AS (SELECT 1 as Id, 'Blue' as Name UNION ALL SELECT 2, 'Red' UNION ALL SELECT 3, 'Green' UNION ALL SELECT 4, 'Yellow')

SELECT p.Name as ProductName, c.Name as Color FROM Production.Product p

CROSS JOIN Colors c
