USE AdventureWorks2017

SELECT *
FROM Person.Person
WHERE FirstName = 'Samantha'
  AND LastName = 'Smith';
