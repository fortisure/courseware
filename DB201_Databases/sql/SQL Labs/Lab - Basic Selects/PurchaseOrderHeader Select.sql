USE AdventureWorks2017

SELECT DATEADD(DAY, 60, OrderDate) AS NotificationDate, ROUND(TotalDue, 2) AS AmountDue
FROM Purchasing.PurchaseOrderHeader WHERE PurchaseOrderID = 100