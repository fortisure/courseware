/*** Select top 10 selling products by total revenue ***/
USE AdventureWorks2017

SELECT TOP 10
    so.ProductID,
    p.Name,
    p.ListPrice,
    COUNT(so.OrderQty) AS TotalCount,
    SUM(so.LineTotal) AS TotalRevenue
FROM Sales.SalesOrderDetail so
    INNER JOIN Production.Product p ON p.ProductID = so.ProductID
GROUP BY 
    so.ProductID, p.Name, p.ListPrice
ORDER BY 
    TotalRevenue DESC

