USE AdventureWorks2017

INSERT INTO
    Production.ProductReview
           (
			 ProductID
			,ReviewerName
			,EmailAddress
			,Rating
			,Comments
		 )
     VALUES
           (
			 782,
			 'Burt Macklin',
			 'burtmacklin@fbi.test',
			 4.5,
			 'This thing is awesome!'
		 )

