/* Returns top 100 products with possible revenue over 500k */
USE AdventureWorks2017
SELECT DISTINCT TOP 100
    p.ProductID, 
    p.Name, 
    p.ProductNumber, 
    p.ListPrice, 
    SUM(pi.Quantity) AS Quantity,
    p.ListPrice * SUM(pi.Quantity) AS PossibleRevenue
FROM Production.Product p
    INNER JOIN Production.ProductInventory pi ON pi.ProductID = p.ProductID
WHERE p.standardcost > 0
GROUP BY 
    p.ProductID, 
    p.Name, 
    p.ProductNumber,
    p.ListPrice
HAVING p.ListPrice * SUM(pi.Quantity) > 500000
ORDER BY PossibleRevenue DESC;
