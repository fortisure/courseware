/* Returns all salaried employees */
USE AdventureWorks2017
SELECT 
    NationalIDNumber,
    LoginID, 
    JobTitle, 
    BirthDate, 
    MaritalStatus, 
    Gender, 
    HireDate,
    VacationHours,
    SickLeaveHours
  FROM HumanResources.Employee
  WHERE SalariedFlag = 1;