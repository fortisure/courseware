/* Updates Last Created Person Name */

USE AdventureWorks2017

UPDATE Person.Person 
    SET FirstName = 'Testy', LastName = 'Testerson'
    WHERE BusinessEntityID = (SELECT MAX(BusinessEntityID) FROM Person.BusinessEntity)